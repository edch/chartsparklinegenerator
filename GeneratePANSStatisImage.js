var moment = require('moment');
var schedule = require('node-schedule');
var Promise = require('bluebird')
var fsx = require('fs-extra');

//chart Generators
var d3 = require('d3');
var techan = require('techan');
var jsdom = require('jsdom');
var svg2img = require('svg2img');
var d3File = fsx.readFileSync('d3.v4.min.js').toString();

var PANSStatic = require('./PANSStatic');

var Pgb = require("pg-bluebird");
var pgb = new Pgb();
var pgConnObj;
var conString = "postgres://datafeed:emptykosong@172.31.2.182/DATAFEED";

//Utility sama seperti get url tapi bedanya ini tanpa base url
function getStaticFileName(stock, interval, urlPrefix)
{
    var x = "";
    if (interval == 'day')
        x = "d";
    else if (interval == 'week')
        x = "w";
    else if (interval == 'month')
        x = "m";
    else 
        x = interval;
    
    return urlPrefix +"static" +stock +x +".png";
}

var jswindow;
jsdom.env({
    html: "<html><head><style></style></head><body></body></html>",
    src: [d3File],
    done:
        function (err, window) {
            if (err)
            {            
                console.log("jsdom error: " +err);
                return;
            }            
            
            jswindow = window;
        }
    });//end of env

function generatePANSStatic()
{
    var dt = new Date();
    var yr = dt.getFullYear();

    return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;
        
        //modify to fetch values inter year
        var sql = "select * from v_" +(yr-1) +"_daily_chart_adjusted where stock = 'PANS' UNION select * from v_" +yr +"_daily_chart_adjusted where stock = 'PANS' order by date desc LIMIT 400";
        //console.log(sql);

        return pgConnObj.client.query(sql);
    })
    .then(function (result) {

        if (result == undefined || result.rows == undefined || result.rows.length <= 0)
        {            
            console.log("empty result!");
            return Promise.resolve([]);
        }
        
        var techanData = [], obj, momObj;
        for (var i = 0; i < result.rows.length; i++)
        {
            obj = {};
               
            momObj = moment(result.rows[i].date);
            if (momObj.isValid())
            {                
                obj.date = momObj.format("D-MMM-YY");

                obj.open = result.rows[i].open;
                obj.high = result.rows[i].high;
                obj.low = result.rows[i].low;
                obj.close = result.rows[i].last;
                obj.tvol = result.rows[i].tvol;
                
                techanData.push(obj);
            }
        }
       
        if (techanData.length <= 0)
        {
            console.log("empty json data!");
            return Promise.resolve([]);
        }
        else
        {           
            var tmp = jswindow.d3.select("body");
            tmp.select("svg").remove();
            tmp.call(PANSStatic(d3, techan, techanData));

            return new Promise(function (resolve, reject) {
                svg2img(jswindow.d3.select("body").html(), function (error, buffer) {
                    if (error)
                        reject(error);

                    var dirName = "./sparklines/";                    
                    var fileName = getStaticFileName("PANS", "", "");
                    console.log("Before save....");
                    return fsx.outputFile(dirName +fileName, buffer)
                        .then(() => {
                            resolve();
                        })
                        .catch( (err) => ( console.log(err) ));
                });
            });       
        }
    });
}

function exitHandler(options, err) {
    if (options.cleanup) console.log('exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

if (process.argv[2] == 'schedule')
{
    console.log("Starting PANS performance chart updater for pans.id...");
    var job1 = schedule.scheduleJob('0 17 * * 1-5', function() {    
        var dt = new Date();
        console.log("Current time is : " +dt);
        console.log('info', 'Begin updating PANS performance chart for pans.id ...'); 
    
        generatePANSStatic().then(function () {
            console.log('Finished.');           
        });
    });   
}
else
{
    generatePANSStatic().then(function () {        
        console.log('Finished generating PANS performance chart.');
        process.exit(0);        
    });
}

process.stdin.resume();//so the program will not close instantly

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
