function RSI(d3, techan, jsonData, usrHeight, usrWidth, strokeColor) {

  var defW = 250, defH = 150, defStrokeColor = "#0099FF";

  if (usrHeight != undefined)
    defH = usrHeight;

  if (usrWidth != undefined)
    defW = usrWidth;

  if (strokeColor != undefined)
    defStrokeColor = strokeColor;

  var margin = {top: 20, right: 20, bottom: 30, left: 30},
      width = defW - margin.left - margin.right, 
      height = defH - margin.top - margin.bottom;
    
  var parseDate = d3.timeParse("%d-%b-%y %H %M");

  var dataLen = jsonData.length;
  var data = jsonData.map(function (d) {    
    return {
      date: parseDate(d.date),
      open: +d.open,
      high: +d.high,
      low: +d.low,
      close: +d.close,
      volume: +d.tvol
    };
  }).sort(function (a, b) {
    return d3.ascending(a.date, b.date);
  });

  var firstDate = data[0].date, endDate = data[dataLen-1].date;
  var tickFormat=function(date) {
    //if(date.getMinutes()) return d3.timeFormat('%H:%M')(date);
    //if(date.getHours()) return d3.timeFormat('%H:%M')(date);
    if(date.getDay()&&date.getDate()!=1) return d3.timeFormat('%b')(date);
    if(date.getDate()!=1) return d3.timeFormat('%e %b')(date);
    //if(date.getMonth()) return d3.timeFormat('%B')(date);
    return d3.timeFormat('%b %Y')(date);
  }

  var area = d3.area()
  .x(function(d) { return x(d.date); })
  .y0(function (d) { 
    if (d.rsi >= 70) return y(d.rsi);
    else if (d.rsi <= 30) return y(30);
    else return y(-1);
  })
  .y1(function (d) { 
    if (d.rsi >= 70) return y(70);
    else if (d.rsi <= 30) return y(d.rsi);
    else return y(-1);
  });
  //.y1(function(d) { return y(70); });

  var area2 = d3.area()
  .x(function(d) { return x(d.date); })
  .y0(function(d) { return y(30); })
  //.y1(function (d) { return d.rsi >= 70? y(d.rsi) : y(0)});
  .y1(function(d) { return y(d.rsi); });

  var x = techan.scale.financetime()
    .domain([firstDate, endDate])
    .range([0, width]);

  var y = d3.scaleLinear()
    .range([height, 0]);
 
  var rsi = techan.plot.rsi()
            .xScale(x)
            .yScale(y);

  var accessor = rsi.accessor();
  var xAxis = d3.axisBottom(x).ticks(5).tickFormat(tickFormat);
  var yAxis = d3.axisLeft(y).ticks(2).tickFormat(d3.format(",.3s"));

  return function(g) {
    var svg = g.append("svg")
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    var ohlcAnnotation = techan.plot.axisannotation()
      .axis(yAxis)
      .orient('left');

    svg.append("g").attr("class", "rsi");

    svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")");

    svg.append("g").attr("class", "y axis")
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")        
        .text("RSI");
    
    //Draw method
    var rsiData = techan.indicator.rsi()(data);
    x.domain(rsiData.map(rsi.accessor().d));
    y.domain(techan.scale.plot.rsi(rsiData).domain());

    svg.selectAll("g.rsi").datum(rsiData).call(rsi);
        
    //svg.selectAll("text").style("fill", "#555");

    svg.selectAll("g.x.axis").call(xAxis);
    svg.selectAll("g.y.axis").call(yAxis);
    //svg.selectAll("g.y.axis path").style("stroke", "red");
    svg.selectAll("path.domain").style("stroke", "#000000");
    svg.selectAll("line").style("stroke", "#000000");
    //svg.style("font", "3px Roboto");
    svg.selectAll("text").style("fill", "#000000");
    
    
    //Annotation    
    var OS = svg.append("g").style("fill", "#DDDDDD")    
    .datum([{value: 70}])
    .call(ohlcAnnotation);    
    OS.selectAll("text").style("fill", "#000000");

    OS = svg.append("g").style("fill", "#DDDDDD")
    .datum([{value: 30}])    
    .call(ohlcAnnotation);
    OS.selectAll("text").style("fill", "#000000");
    
    //Apply styles here    
    //svg.selectAll("path.line").style("fill", "none").style("stroke",defStrokeColor).style("stroke-width", "2");
    svg.selectAll("path.rsi").style("fill", "none").style("stroke-width","1").style("stroke", "#FF4500");    
    svg.selectAll("path.overbought").style("stroke", "#FF0000").style("solid", "3, 3");
    svg.selectAll("path.oversold").style("stroke", "#4582E7").style("solid", "3, 3");
    svg.selectAll("path.middle").style("stroke", "#AAAAAA").style("stroke-dasharray", "5, 5");
    //svg.selectAll("g.x.axis").style("stroke", "#555555").style("solid", "5, 5");    
  }
}

// If we're in node
if(typeof module === 'object') {
  module.exports = RSI;
}

