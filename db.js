var pg = require("pg");

var pgConfig = {
  user: 'datafeed', //env var: PGUSER
  database: 'DATAFEED', //env var: PGDATABASE
  password: 'emptykosong', //env var: PGPASSWORD
  host: '172.31.2.182', // Server hosting the postgres database
  port: 5432, //env var: PGPORT
  max: 4 // max number of clients in the pool  
};

pg.defaults.poolSize = 4;

var conString = "postgres://datafeed:emptykosong@172.31.2.182/DATAFEED", pool;
pg.defaults.connectionString = conString;


/*PG ASYNC CONF*/
var Pgb = require("pg-bluebird");
var pgb = new Pgb();
var pgConnObj;

module.exports.init = function (callback) 
{
  pool = new pg.Pool(pgConfig);

  pool.on('error', function (err, client) 
  {
    console.error('Idle client error', err.message, err.stack)
  });

  return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;
        return Promise.resolve();
    });
};

module.exports.doQuery = function (txt, vals, cb) 
{
  pool.connect(function(err, client, done)
  {
    if(err)
    {
      console.error('error fetching client from pool', err);
      cb(err, result);
      done();
      return;
    }
        
    client.query(txt, vals, function(err2, result) 
    {
      cb(err2, result);

      //call `done()` to release the client back to the pool
      done();
          
      if(err2) {
        return console.error('error running PG query', err2);
      } 
    });
  });
};

module.exports.pgb = pgb;
module.exports.doQuery2 = function (txt) //return promise
{
  //return new Promise()
  return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;
        return pgConnObj.client.query(txt);
    })  
};

module.exports.cleanUp = function ()
{
  pgp.end();
};