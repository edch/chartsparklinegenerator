function MACDCross(d3, techan, jsonData, usrHeight, usrWidth, strokeColor) {
  var defW = 250, defH = 150, defStrokeColor = "#0099FF";

  if (usrHeight != undefined)
    defH = usrHeight;

  if (usrWidth != undefined)
    defW = usrWidth;

  if (strokeColor != undefined)
    defStrokeColor = strokeColor;

  var margin = {top: 10, right: 5, bottom: 25, left: 50},
      width = defW - margin.left - margin.right,
      height = defH - margin.top - margin.bottom;
    
  var parseDate = d3.timeParse("%d-%b-%y %H %M");

  var dataLen = jsonData.length;  

  //Slice
  //if (dataLen > 80)
    //jsonData = jsonData.slice(0, 80);
  //else
    //jsonData = jsonData.slice(0, dataLen);

  var data = jsonData.map(function (d) {
    return {
      date: parseDate(d.date),
      open: +d.open,
      high: +d.high,
      low: +d.low,
      close: +d.close,
      volume: +d.tvol
    };
  }).sort(function (a, b) {
    return d3.ascending(a.date, b.date);
  });
  
  var firstDate = data[0].date, endDate = data[data.length-1].date;
  var tickFormat=function(date) {
    //if(date.getMinutes()) return d3.timeFormat('%H:%M')(date);
    //if(date.getHours()) return d3.timeFormat('%H:%M')(date);
    if(date.getDay()&&date.getDate()!=1) return d3.timeFormat('%e %B')(date);
    if(date.getDate()!=1) return d3.timeFormat('%e %B')(date);
    //if(date.getMonth()) return d3.timeFormat('%B')(date);
    return d3.timeFormat('%B %Y')(date);
  }  

  return function(g) {
    var svg = g.append("svg")
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g").attr("class", "macd");

    svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")");

    svg.append("g").attr("class", "y axis")
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("MACD");        
    
    //Draw method
    var macdData = techan.indicator.macd()(data);

    var x = techan.scale.financetime()
    .domain([firstDate, endDate])
    .range([0, width]);

    var y = d3.scaleLinear()      
      .range([height, 0]);
    
    var macd = techan.plot.macd()
              .xScale(x)
              .yScale(y);

    var accessor = macd.accessor();
    var xAxis = d3.axisBottom(x).ticks(3).tickFormat(tickFormat);
    var yAxis = d3.axisLeft(y).ticks(5).tickFormat(d3.format(",.3s"));

    x.domain(macdData.map(macd.accessor().d));

    //Find Max 1
    //Pilih nilai terbesar untuk domain
    //y.domain(techan.scale.plot.macd(macdData).domain());   
    var maxY = [], minY = [];
    var maxSig = Math.max.apply(Math, macdData.map(function(v) {      
      return v.signal;
    }));

    var maxDiff = Math.max.apply(Math, macdData.map(function(v) {
      return v.difference;
    }));

    var maxMACD = Math.max.apply(Math, macdData.map(function(v) {      
      return v.macd;
    }));

    maxY.push(maxSig);
    maxY.push(maxDiff);
    maxY.push(maxMACD);
    
    var minSig = Math.min.apply(Math, macdData.map(function(v) {      
      return v.signal;
    }));

    var minDiff = Math.min.apply(Math, macdData.map(function(v) {      
      return v.difference;
    }));

    var minMACD = Math.min.apply(Math, macdData.map(function(v) {
      return v.macd;
    }));

    minY.push(minSig);
    minY.push(minDiff);
    minY.push(minMACD);

    var topY = Math.max(...maxY);
    var bottomY = Math.min(...minY);

    var offset;
    if (topY >= 100 || bottomY <= -100)
      offset = 8;
    else 
      offset = 2;

    y.domain([bottomY - offset, topY + offset]);

    svg.selectAll("g.macd").datum(macdData).call(macd);
    svg.selectAll("g.x.axis").call(xAxis);
    svg.selectAll("g.y.axis").call(yAxis);
    svg.style("font", "3px sans-serif");
    svg.selectAll("text").style("fill", "#000");

    //Apply styles here    
    //svg.selectAll("path.line").style("fill", "none").style("stroke",defStrokeColor).style("stroke-width", "2");        
    svg.selectAll("path").style("fill", "none").style("stroke-width","1");
    svg.selectAll("path.macd").style("stroke", "#0000AA");
    svg.selectAll("path.signal").style("stroke", "#FF1111");
    svg.select("path.zero").style("stroke", "#555555").style("stroke-dasharray", "0.7");
    svg.select("path.difference").style("fill", "#444444").style("opacity", "0.6");
    //Draw legends here?
  }
}

// If we're in node
if(typeof module === 'object') {
  module.exports = MACDCross;
}