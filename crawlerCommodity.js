var schedule = require("node-schedule");
var conString = "postgres://datafeed:emptykosong@172.31.2.182/DATAFEED";
var Promise = require("bluebird");
var Crawler = require("crawler");
var Pgb = require("pg-bluebird");

var pgb = new Pgb();
var pgConnObj;

var c = new Crawler({
    maxConnections : 1,
    
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            //console.log($("title").text());
            console.log("Global crawler callback");
        }
        done();
    }
});

c.queue([{
  uri: 'https://www.quandl.com/collections/markets/commodities',
  rateLimit: 1000,
  callback: function (error, res, done) 
  {
    if (error)
      console.log(error);
    else
    {
      console.log(res.body);
    }

    done();
  }
}]);
