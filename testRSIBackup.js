var fsx = require('fs-extra');
var d3 = require('d3');
var techan = require('techan');
var RSI = require('./rsi');
var jsdom = require('jsdom');
var svg2img = require('svg2img');
var moment = require('moment');
var d3File = fsx.readFileSync('d3.v4.min.js').toString();
var schedule = require('node-schedule');
var Promise = require('bluebird')

//REDIS
/*REDIS CONF*/
var redis = require('redis');
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
client = redis.createClient(); //Select default database (0)

client.on("error", function (err) {
    //console.log("Redis Error: " + err);
});

//TECHNICAL ANALYSIS LIBRARY
var talib = require('./node_modules/talib/build/Release/talib');
console.log("TALib Version: " + talib.version);

//Utility sama seperti get url tapi bedanya ini tanpa base url
function getSparklineFileName(stock, interval, urlPrefix)
{
    var x;
    if (interval == 'day')
        x = "d";
    else if (interval == 'week')
        x = "w";
    else if (interval == 'month')
        x = "m";
    else if (interval == 'RSIOB')
        x = interval;
    else if (interval == 'RSIOS')
        x = interval;
    else if (interval == 'RSINE')
        x = interval;
    else return "";

    return urlPrefix +"sl" +stock +x +".png";
}

//PG
var Pgb = require("pg-bluebird");
var pgb = new Pgb(); //test if this can be reused
var pgConnObj;
var conString = "postgres://datafeed:emptykosong@172.31.2.182/DATAFEED";//Initialize jsdom

var jswindow;
jsdom.env({
    html: "<html><head><style></style></head><body></body></html>",
    src: [d3File],
    done:
        function (err, window) {
            if (err)
            {            
                console.log("jsdom error: " +err);
                return;
            }            
            
            jswindow = window;
        }
    });//end of env

//TALIB callers
function handleMA(arrData, techanFmtData, param, stCode)
{
    return new Promise(function (resolve, reject) {
        talib.execute({
            name: "MA",
            inReal: arrData,
            startIdx: 0,
            endIdx: arrData.length-1,
            optInTimePeriod: param
        },
        function (err, result) {
            if (err)
            {
                console.log(err);
                return reject(err);
            }
            resolve(result);
        });
    })
    .then( function (data) {        
        if (data.result.outReal.length > 0) {
            //Determine RSI threshold here
            // > 70 OB
            // < 30 OS
            var threshold = 3;
            var rsiVal = data.result.outReal[data.result.outReal.length-1];
            
            if (rsiVal <= 30+threshold) { //OS                
                client.zadd("RSIOS", +rsiVal, stCode, function (err, res) { 
                });

                console.log(stCode +" is Oversold");
                return Promise.resolve("RSIOS");
            }
            else if (rsiVal >= 70-threshold) { //OB
                client.zadd("RSIOB", +rsiVal, stCode, function (err, res) {                     
                });
                console.log(stCode +" is Overbought");
                return Promise.resolve("RSIOB");
            }
            else 
            {
                client.zadd("RSINE", +rsiVal, stCode, function (err, res) {                     
                });
                console.log(stCode +" is neutral");
                return Promise.resolve("RSINE");
            }
        }
        else return Promise.resolve("Failed");
    })
    .then( function (szRes) {        
        //Only receive jsonFormatted data.
        if (techanFmtData && szRes != "Failed")
        {            
            var tmp = jswindow.d3.select("body");
            tmp.select("svg").remove();
            tmp.call(RSI(d3, techan, techanFmtData));
            return new Promise(function (resolve, reject) {
                svg2img(jswindow.d3.select("body").html(), function (error, buffer) {                    
                    if (error)
                        reject(error);

                    var dirName = "./RSI/";
                    if (szRes == 'RSIOS')
                        dirName += 'Oversold/';
                    else if (szRes == 'RSIOB')
                        dirName += 'Overbought/';
                    else if (szRes == 'RSINE')
                        dirName += 'Neutral/';

                    var fileName = getSparklineFileName(stCode, szRes, "");

                    //console.log(dirName +fileName);
                    
                    return fsx.outputFile(dirName +fileName, buffer)
                        .then(() => { //console.log(dirName +fileName); 
                            resolve();
                        })
                        .catch( (err) => ( console.log(err) ));
                });
            });
        } 
        else return Promise.resolve();
    });
}

function handleRSI(arrData, techanFmtData, param, stCode)
{
    return new Promise(function (resolve, reject) {
        talib.execute({
            name: "RSI",
            inReal: arrData,
            startIdx: 0,
            endIdx: arrData.length-1,
            optInTimePeriod: param
        },
        function (err, result) {
            if (err)
            {
                console.log(err);
                return reject(err);
            }
            resolve(result);
        });
    })
    .then( function (data) {        
        if (data.result.outReal.length > 0) {
            //Determine RSI threshold here
            // > 70 OB
            // < 30 OS
            var threshold = 3;
            var rsiVal = data.result.outReal[data.result.outReal.length-1];
            
            if (rsiVal <= 30+threshold) { //OS                
                client.zadd("RSIOS", +rsiVal, stCode, function (err, res) { 
                });

                console.log(stCode +" is Oversold");
                return Promise.resolve("RSIOS");
            }
            else if (rsiVal >= 70-threshold) { //OB
                client.zadd("RSIOB", +rsiVal, stCode, function (err, res) {                     
                });
                console.log(stCode +" is Overbought");
                return Promise.resolve("RSIOB");
            }
            else 
            {
                client.zadd("RSINE", +rsiVal, stCode, function (err, res) {                     
                });
                console.log(stCode +" is neutral");
                return Promise.resolve("RSINE");
            }
        }
        else return Promise.resolve("Failed");
    })
    .then( function (szRes) {        
        //Only receive jsonFormatted data.
        if (techanFmtData && szRes != "Failed")
        {            
            var tmp = jswindow.d3.select("body");
            tmp.select("svg").remove();
            tmp.call(RSI(d3, techan, techanFmtData));
            return new Promise(function (resolve, reject) {
                svg2img(jswindow.d3.select("body").html(), function (error, buffer) {                    
                    if (error)
                        reject(error);

                    var dirName = "./RSI/";
                    if (szRes == 'RSIOS')
                        dirName += 'Oversold/';
                    else if (szRes == 'RSIOB')
                        dirName += 'Overbought/';
                    else if (szRes == 'RSINE')
                        dirName += 'Neutral/';

                    var fileName = getSparklineFileName(stCode, szRes, "");

                    //console.log(dirName +fileName);
                    
                    return fsx.outputFile(dirName +fileName, buffer)
                        .then(() => { //console.log(dirName +fileName); 
                            resolve();
                        })
                        .catch( (err) => ( console.log(err) ));
                });
            });
        } 
        else return Promise.resolve();
    });
}

function loadAndFormatDataTALib(code, numDays, indName)
{
    if (!code || !numDays)
        return Promise.resolve([]);   
        
    var maxRec, viewName, sql = '';
    var dt = new Date();
    var nowMoment = moment();
    var yr = dt.getFullYear(), yr2;

    if (numDays > 0)
    {
        //Trading day sebulan
        maxRec = numDays;
        //Daily selama sebulan terakhir //atau beginning from last month?
        viewName = "_daily";
        yr2 = nowMoment.subtract(maxRec, 'd').year();
    }    
    else return Promise.resolve([]);    
    
    if (yr == yr2)
        sql += "SELECT date, open, high, low, last, tvol from v_" +yr +"_daily_chart_adjusted WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +numDays;
    else
    {                    
        sql += "SELECT date, open, high, low, last, tvol from ( SELECT date, open, high, low, last, tvol from v_" +yr +"_daily_chart_adjusted WHERE stock = '" +code +"'"
        sql += " UNION ALL SELECT date, open, high, low, last, tvol from v_" +(yr-1) +"_daily_chart_adjusted WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxDays +") x;";
    }
    
    //Date,Open,High,Low,Close,Volume
    return pgConnObj.client.query(sql).then(function (result) 
    {
        if (result == undefined || result.rows == undefined || result.rows.length <= 0)
        {            
            return Promise.resolve([]);
        }

        var arrData = [];
        for (var i = result.rows.length-1; i >= 0; i--)
            arrData.push(result.rows[i].last);

        var techanData = [], obj, momObj;
        for (var i = 0; i < result.rows.length; i++)
        {
            obj = {};
               
            momObj = moment(result.rows[i].date);
            if (momObj.isValid())
            {                
                obj.date = momObj.format("D-MMM-YY HH mm");
                obj.open = result.rows[i].open;
                obj.high = result.rows[i].high;
                obj.low = result.rows[i].low;
                obj.close = result.rows[i].last;
                obj.tvol = result.rows[i].tvol;
                
                techanData.push(obj);
            }
        }
       
        if (arrData.length <= 0)
        {
            console.log("empty json data!");
            return Promise.resolve([]);
        }
        else
        {
            if (indName == 'RSI')
            {
                var rsiParam = 14;                
                return handleRSI(arrData, techanData, rsiParam, code);
            }
            else return Promise.resolve([]);
        }
    });
}

//Load data here
function loadAndFormatData(code, interval)
{
    if (!code || !interval)
        return Promise.resolve([]);   
        
    var maxRec, viewName;
    var dt = new Date();
    var nowMoment = moment();
    var yr = dt.getFullYear(), yr2;

    //Set the proper interval
    if (interval == 'd')
    {
        //Trading day per week is at most 5 days * 7 hours per day
        //Per jam selama 1 minggu?
        
        //maxRec = maxDays * 7;
        maxRec = 7 //approx 60 mins * 6 hrs, per 5 mins chart => 360 / 72

        //viewName = "_hourly";
        viewName = "_intraday";
        //yr2 = nowMoment.subtract(7, 'd').year();
        yr2 = yr;
    }
    else if (interval == 'm')
    {
        //Trading day sebulan
        maxRec = 22;
        //Daily selama sebulan terakhir //atau beginning from last month?
        viewName = "_daily";
        yr2 = nowMoment.subtract(30, 'd').year();
    }
    else if (interval == 'y')
    {
        //Approx trading day setahun
        maxRec = 200;
        //atau weekly?
        viewName = "_daily";
        yr2 = nowMoment.subtract(360, 'd').year();
    }
    else return Promise.resolve([]);
    
    if (yr != yr2)
    {
        if (interval == 'd')
        {
            sql = "SELECT * from ( SELECT date, open, high, low, last, tvol from v_stock_intraday_chart WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec;
            sql += " UNION ALL SELECT date, open, high, low, last, tvol from v_stock_intraday_chart WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec +") x;";
        }
        else
        {            
            sql = "SELECT * from ( SELECT date, open, high, low, last, tvol from v_" +yr +viewName +"_chart_adjusted WHERE stock = '" +code +"' ";
            sql += " UNION ALL SELECT date, open, high, low, last, tvol from v_" +(yr-1) +viewName +"_chart_adjusted WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec +") x;";
            //console.log(sql);
        }
    }
    else
    {
        if (interval == 'd')
        {
            sql = "SELECT date_trunc('hour'::text, t.date) + (date_part('minute'::text, t.date)::integer / 5)::double precision * '00:05:00'::interval AS date, \
                    t.stock, (array_agg(t.open ORDER BY t.date))[1] AS open, max(t.high) AS high, min(t.low) AS low, \
                    (array_agg(t.last ORDER BY t.date DESC))[1] AS last, sum(t.tvol) AS tvol, sum(t.freq) AS freq \
                    FROM v_stock_intraday_chart t WHERE t.stock = '" +code +"' GROUP BY (date_trunc('hour'::text, t.date) + (date_part('minute'::text, t.date)::integer / 5)::double precision * '00:05:00'::interval), t.stock LIMIT " +maxRec;
        }
        else 
        {
            sql = "SELECT date, open, high, low, last, tvol from v_" +yr +viewName +"_chart_adjusted WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec;            
        }
    }
    
    //Date,Open,High,Low,Close,Volume
    return pgConnObj.client.query(sql).then(function (result) 
    {
        if (result == undefined || result.rows == undefined || result.rows.length <= 0)
        {            
            return Promise.resolve([]);
        }

        var jsonData = [], obj, momObj;
        for (var i = 0; i < result.rows.length; i++)
        {
            obj = {};
               
            momObj = moment(result.rows[i].date);
            if (momObj.isValid())
            {                
                obj.date = momObj.format("D-MMM-YY HH mm");
                obj.open = result.rows[i].open;
                obj.high = result.rows[i].high;
                obj.low = result.rows[i].low;
                obj.close = result.rows[i].last;
                obj.tvol = result.rows[i].tvol;
                jsonData.push(obj);
            }
        }        
       
        if (jsonData.length <= 0)
        {
            console.log("empty json data!");                        
        }
        else
        {            
            var tmp = jswindow.d3.select("body");
            tmp.select("svg").remove();            
            tmp.call(RSI(d3,techan, jsonData));
            return new Promise(function (resolve, reject) {
                svg2img(jswindow.d3.select("body").html(), function (error, buffer) {                    
                    if (error)
                        reject(error);

                    var dirName = "./images/";
                    var fileName = getSparklineFileName(code, interval, "");

                    return fsx.outputFile(dirName + fileName, buffer)
                        .then(() => { console.log(dirName + fileName); resolve();})
                        .catch( (err) => ( console.log(err) ));
                });
            });
        }        
    });    
}

//DRIVER Program
function generateAndCalcRSI(rsIParam)
{
    return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;
        return pgConnObj.client.query("select stock from stock_list order by stock");
    })
    .then(function (result) {
        var code;
        var stockList = [];
        
        for (var i = 0; i < result.rows.length; i++)
        {
            code = result.rows[i].stock.trim();
            if (code.indexOf("-W") < 0 && code.indexOf("-R") < 0)
                stockList.push(code);
        }

        return stockList;
    })
    .then(function (stList) {
        return stList.reduce(function (pm, key) {
            return pm.then(function () {                
                return loadAndFormatDataTALib(key, 100, 'RSI');                
            })
        }, Promise.resolve());
    })
    .then(function() {
        console.log('Finished');
        //draw image here?        
        pgConnObj.done();

        //Delay to let the IO operation completes?
        return Promise.resolve();
    })
    .catch(function (error)
    {
        console.log(error);
        return Promise.reject();
    });
}

var function_desc = talib.explain("RSI");
console.dir(function_desc);
//generateAndCalcRSI();

function exitHandler(options, err) {    
    if (options.cleanup) console.log('exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

if (process.argv[2] == 'schedule')
{
    console.log('Calculating RSI...');   
}
else
{
    if (process.argv[2] == 'GenerateRSI')
    {
        console.log('Calculating RSI...');  
        generateAndCalcRSI().then(function () {
            console.log('Finished generating RSI');
            process.exit(0);
        });
    }   
}

process.stdin.resume();//so the program will not close instantly

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
