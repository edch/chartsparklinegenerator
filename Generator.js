//Programming frameworks
var moment = require('moment');
var schedule = require('node-schedule');
var Promise = require('bluebird')
var fsx = require('fs-extra');

//chart Generators
var d3 = require('d3');
var techan = require('techan');
var jsdom = require('jsdom');
var svg2img = require('svg2img');
var d3File = fsx.readFileSync('d3.v4.min.js').toString();

//D3/Techan realtedTechnical indicators
var RSI = require('./rsi');
var MACD = require('./MACDCross');

/*REDIS CONF*/
var redis = require('redis');
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
client = redis.createClient(); //Select default database (0)

client.on("error", function (err) {
    //console.log("Redis Error: " + err);
});

var slPrefix = "./sparklines";

//TEMPORARY SOLUTION!
var lq45List = ['AALI','ADHI', 'ADRO', 'AKRA', 'ANTM', 'ASII', 'ASRI', 'BBCA', 'BBNI' ,'BBRI', 'BBTN', 'BMRI', 'BSDE', 'BUMI', 'CPIN', 'ELSA', 'EXCL', 'GGRM', 'HMSP', 'ICBP', 'INCO', 'INDF', 'INTP', 'JSMR', 'KLBF', 'LPKR', 'LPPF', 'LSIP', 'MNCN', 'MYRX', 'PGAS', 'PPRO', 'PTBA', 'PTPP', 'PWON', 'SCMA', 'SMGR', 'SMRA', 'SRIL', 'SSMS', 'TLKM', 'UNTR', 'UNVR', 'WIKA', 'WSKT'];
var topStockList = ['AALI','ADHI', 'ADRO', 'AKRA', 'ANTM', 'ASII', 'ASRI', 'BBCA', 'BBNI' ,'BBRI', 'BBTN', 'BMRI', 'BSDE', 'BUMI', 'CPIN', 'ELSA', 'EXCL', 'GGRM', 'HMSP', 'ICBP', 'INCO', 'INDF', 'INTP', 'JSMR', 'KLBF', 'LPKR', 'LPPF', 'LSIP', 'MNCN', 'MYRX', 'PGAS', 'PPRO', 'PTBA', 'PTPP', 'PWON', 'SCMA', 'SMGR', 'SMRA', 'SRIL', 'SSMS', 'TLKM', 'UNTR', 'UNVR', 'WIKA', 'WSKT',
    'BMTR', 'MPPA', 'SILO', 'ASRI', 'AISA', 'BNGA', 'BSDE', 'CTRA', 'MNCN', 'PGAS', 'DOID', 'INDY', 'HRUM', 'INKP', 'ERAA', 'CTRP', 'GJTL', 'LSIP', 'ISAT', 'INCO', 'MEDC', 'KIJA', 'MAPI', 'SMGR', 'SRIL', 'TOTL', 'PTPP', 'SCMA', 'SOCI', 'WIKA', 'SIMP', 'AGRO', 'BWPT', 'BJTM', 'BJBR', 'BDMN', 'ACES', 'BIRD', 'GIAA', 'KBLI', 'BRPT', 'NIKL', 'TRAM', 'LEAD', 'LPCK', 'RIMO', 'HRTA', 
    'ARMY', 'FIRE', 'HOKI', 'WSBP', 'BKSL', 'FPNI', 'WTON', 'INDF', 'MARK', 'FORZ', 'TPIA', 'MYRX', 'TOPS', 'ASBI', 'ENRG', 'BRMS', 'BIPI', 'KRAS', 'MCOR', 'MDLN', 'POOL', 'BNLI', 'BOGA', 'MYOR', 'TAXI', 'KAEF', 'FINN'];

//Experimental, to let the I/O finished after we finished processing everything!
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

//TECHNICAL ANALYSIS LIBRARY
var talib = require('./node_modules/talib/build/Release/talib');
console.log("TALib Version: " + talib.version);

//Utility sama seperti get url tapi bedanya ini tanpa base url
function getSparklineFileName(stock, interval, urlPrefix)
{
    var x = "";
    if (interval == 'day')
        x = "d";
    else if (interval == 'month')
        x = "m";
    else if (interval == 'year')
        x = "y";
    else 
        x = interval;
    
    return urlPrefix +"sl" +stock +x +".png";
}

//PG
var Pgb = require("pg-bluebird");
var pgb = new Pgb(); //test if this can be reused
var pgConnObj;
var conString = "postgres://datafeed:emptykosong@172.16.19.24/pgdb";//Initialize jsdom

var jswindow;
jsdom.env({
    html: "<html><head><style></style></head><body></body></html>",
    src: [d3File],
    done:
        function (err, window) {
            if (err)
            {            
                console.log("jsdom error: " +err);
                return;
            }            
            
            jswindow = window;
        }
    });//end of env

//TALIB callers
function handleMACDCross(arrData, techanFmtData, fastPer, slowPer, signalPer, stCode)
{
    return new Promise(function (resolve, reject) {
        talib.execute({
            name: "MACD",
            inReal: arrData,
            startIdx: 0,
            endIdx: arrData.length-1,
            optInFastPeriod: fastPer,
            optInSlowPeriod: slowPer,
            optInSignalPeriod: signalPer,
        },
        function (err, result) {
            if (err)
            {
                console.log(err);
                return reject(err);
            }
            resolve(result);
        });
    })
    .then( function (data) {
        if (data.result.outMACD.length > 2 && data.result.outMACDSignal.length > 2 && data.result.outMACDHist.length > 2) {
            var threshold = 0.2, currDayDiff = data.result.outMACD[data.result.outMACD.length-1] - data.result.outMACDSignal[data.result.outMACDSignal.length-1];
            
            var currDaySign = (data.result.outMACD[data.result.outMACD.length-1] - data.result.outMACDSignal[data.result.outMACDSignal.length-1]) >= 0 ? 1:-1,
                lastDaySign = (data.result.outMACD[data.result.outMACD.length-2] - data.result.outMACDSignal[data.result.outMACDSignal.length-2]) >= 0 ? 1:-1;
            
            //Untuk cek crossover, cek 2 hari terakhir. Kalau posisi +/- antara signal dan macd berubah, berarti ada cross!
            //Kalau tidak ada cross, tapi selisihnya < threshold, consider this is a possible crossover?
            var score, rec;
            
            if (topStockList.indexOf(stCode) > -1)
            {
                score = 10;
            }
            else score = 1;
           
            if (currDaySign == lastDaySign) //No crossover detected, so we check those who come near enough to the threshold
            {
                if (Math.abs(currDayDiff) < threshold)
                {
                    //Mau break positive                   
                    if (data.result.outMACD[data.result.outMACD.length-1] < data.result.outMACDSignal[data.result.outMACDSignal.length-1])
                    {
                        //client.sadd("MACDCrossPos", stCode, function (err, res) {
                        //});
                        
                        rec = ['MACDCrossPos', score, stCode];

                        client.zaddAsync(rec).then(function (res) {
                            console.log(stCode +" wanna MACD Positive Crossover");
                        });
                       
                        return Promise.resolve("CrossPos");
                    }
                    else //Mau break negative
                    {
                        //client.sadd("MACDCrossNeg", stCode, function (err, res) {
                        //});
                        
                        rec = ['MACDCrossNeg', score, stCode];

                        client.zaddAsync(rec).then(function (res) {
                            console.log(stCode +" wanna MACD Negative Crossover");
                        });
                        
                        return Promise.resolve("CrossNeg");
                    }
                }
                else return Promise.resolve("NotCross");
            }
            else //ada crossover, either it's positive or negative
            {
                score += 10;

                if (currDaySign > lastDaySign) //positive crossover
                {
                    //client.sadd("MACDCrossPos", stCode, function (err, res) {
                    //}); 
                    rec = ['MACDCrossPos', score, stCode];
                    
                    client.zaddAsync(rec).then(function (res) {
                        console.log(stCode +" MACD Positive Crossover");
                    });
                    
                    return Promise.resolve("CrossPos");
                }
                else //negative crossover
                {
                    //client.sadd("MACDCrossNeg", stCode, function (err, res) {
                    //});
                    rec = ['MACDCrossNeg', score, stCode];

                    client.zaddAsync(rec).then(function (res) {
                        console.log(stCode +" MACD Negative Crossover");
                    });
                    
                    return Promise.resolve("CrossNeg");
                }
            }                       
        }
        else return Promise.resolve("Failed");
    })
    .then( function (szRes) {
        //Only receive jsonFormatted data.
        if (techanFmtData && szRes != "Failed")
        {
            if (szRes != "NotCross")
            {
                var tmp = jswindow.d3.select("body");
                tmp.select("svg").remove();
                tmp.call(MACD(d3, techan, techanFmtData));
                return new Promise(function (resolve, reject) {
                    svg2img(jswindow.d3.select("body").html(), function (error, buffer) {
                        if (error)
                            reject(error);

                        var dirName = slPrefix +"/MACDCross/";
                        var fileName = getSparklineFileName(stCode, "MACD", "");
                        
                        return fsx.outputFile(dirName +fileName, buffer)
                            .then(() => { //console.log(dirName +fileName); 
                                resolve();
                            })
                            .catch( (err) => ( console.log(err) ));
                    });
                });
            }
        } 
        else return Promise.resolve();
    });
}

function handleRSI(arrData, techanFmtData, param, stCode)
{
    return new Promise(function (resolve, reject) {
        talib.execute({
            name: "RSI",
            inReal: arrData,
            startIdx: 0,
            endIdx: arrData.length-1,
            optInTimePeriod: param
        },
        function (err, result) {
            if (err)
            {
                console.log(err);
                return reject(err);
            }
            resolve(result);
        });
    })
    .then( function (data) {        
        if (data.result.outReal.length > 0) {            
            
            //Determine RSI threshold here
            // > 70 OB
            // < 30 OS
            var threshold = 3, rec, score = 0;
            var rsiVal = data.result.outReal[data.result.outReal.length-1];

            if (topStockList.indexOf(stCode) > -1)
                score = 100;
            //else score = 1;
            
            if (rsiVal <= 30+threshold) { //OS
                //client.zadd("RSIOS", +rsiVal, stCode, function (err, res) { 
                //});
                rec = ['RSIOS', rsiVal+score, stCode];
                
                client.zaddAsync(rec).then(function (res) {
                    console.log(stCode +" is Oversold");
                });
                
                return Promise.resolve("RSIOS");
            }
            else if (rsiVal >= 70-threshold) { //OB
                //client.zadd("RSIOB", +rsiVal, stCode, function (err, res) {
                //});

                rec = ['RSIOB', rsiVal+score, stCode];
                
                client.zaddAsync(rec).then(function (res) {
                    console.log(stCode +" is Overbought");
                });
                
                return Promise.resolve("RSIOB");
            }
            else 
            {
                //client.zadd("RSINE", +rsiVal, stCode, function (err, res) {
                //});
                rec = ['RSINE', rsiVal+score, stCode];
                
                client.zaddAsync(rec).then(function (res) {
                    console.log(stCode +" is neutral");
                });                
                
                return Promise.resolve("RSINE");
            }        
        }
        else return Promise.resolve("Failed");
    })
    .then( function (szRes) {        
        //Only receive jsonFormatted data.
        if (techanFmtData && szRes != "Failed")
        {            
            var tmp = jswindow.d3.select("body");
            tmp.select("svg").remove();
            tmp.call(RSI(d3, techan, techanFmtData));
            return new Promise(function (resolve, reject) {
                svg2img(jswindow.d3.select("body").html(), function (error, buffer) {                    
                    if (error)
                        reject(error);

                    var dirName = slPrefix +"/RSI/";
                    if (szRes == 'RSIOS')
                        dirName += 'Oversold/';
                    else if (szRes == 'RSIOB')
                        dirName += 'Overbought/';
                    else if (szRes == 'RSINE')
                        dirName += 'Neutral/';

                    var fileName = getSparklineFileName(stCode, szRes, "");
                    //console.log(dirName +fileName);
                    return fsx.outputFile(dirName +fileName, buffer)
                        .then(() => { //console.log(dirName +fileName); 
                            resolve();
                        })
                        .catch( (err) => ( console.log(err) ));
                });
            });
        } 
        else return Promise.resolve();
    });
}

function loadAndFormatDataTALib(code, numDays, indName)
{
    if (!code || !numDays)
        return Promise.resolve([]);
        
    var maxRec, viewName, sql = '';
    var dt = new Date();
    var nowMoment = moment();
    var yr = dt.getFullYear(), yr2;

    if (numDays > 0)
    {
        //Trading day sebulan
        maxRec = numDays;
        //Daily selama sebulan terakhir //atau beginning from last month?
        viewName = "_daily";        
    }    
    else return Promise.resolve([]);
    
    //misal max days 200, tapi setelah dikurangi 200 hari, masih dalam tahun yang sama, jadi kita tetap harus traverse ke tahun sebelumnya kalau dia masih belum mencapai 200 di current year!
    
    sql += "SELECT date, open, high, low, last, vol from chart_daily_stock_adjusted WHERE stock_code = '" +code +"' ORDER BY DATE DESC LIMIT " +numDays;
    
    //Date,Open,High,Low,Close,Volume
    return pgConnObj.client.query(sql).then(function (result) 
    {
        if (result == undefined || result.rows == undefined || result.rows.length <= 0)
        {            
            return Promise.resolve([]);
        }

        var arrData = [];
        for (var i = result.rows.length-1; i >= 0; i--)
            arrData.push(result.rows[i].last);

        var techanData = [], obj, momObj;
        for (var i = 0; i < result.rows.length; i++)
        {
            obj = {};
               
            momObj = moment(result.rows[i].date);
            if (momObj.isValid())
            {                
                obj.date = momObj.format("D-MMM-YY HH mm");
                obj.open = result.rows[i].open;
                obj.high = result.rows[i].high;
                obj.low = result.rows[i].low;
                obj.close = result.rows[i].last;
                obj.tvol = result.rows[i].vol;
                
                techanData.push(obj);
            }
        }
       
        if (arrData.length <= 0)
        {
            console.log("empty json data!");
            return Promise.resolve([]);
        }
        else
        {
            if (indName == 'RSI')
            {
                var rsiParam = 14;
                return handleRSI(arrData, techanData, rsiParam, code);
            }
            else if (indName == 'MACDCross')
            {
                return handleMACDCross(arrData, techanData, 12, 26, 9, code);
            }
            else return Promise.resolve([]);
        }
    });
}

//Load data here
function loadAndFormatData(code, interval)
{
    if (!code || !interval)
        return Promise.resolve([]);   
        
    var maxRec, viewName;
    var dt = new Date();
    var nowMoment = moment();
    var yr = dt.getFullYear(), yr2;

    //Set the proper interval
    if (interval == 'd')
    {
        //Trading day per week is at most 5 days * 7 hours per day
        //Per jam selama 1 minggu?
        
        //maxRec = maxDays * 7;
        maxRec = 7 //approx 60 mins * 6 hrs, per 5 mins chart => 360 / 72

        //viewName = "_hourly";
        viewName = "_intraday";
        //yr2 = nowMoment.subtract(7, 'd').year();
        yr2 = yr;
    }
    else if (interval == 'm')
    {
        //Trading day sebulan
        maxRec = 22;
        //Daily selama sebulan terakhir //atau beginning from last month?
        viewName = "_daily";
        yr2 = nowMoment.subtract(30, 'd').year();
    }
    else if (interval == 'y')
    {
        //Approx trading day setahun
        maxRec = 200;
        //atau weekly?
        viewName = "_daily";
        yr2 = nowMoment.subtract(360, 'd').year();
    }
    else return Promise.resolve([]);
    
    if (yr != yr2)
    {
        if (interval == 'd')
        {
            sql = "SELECT * from ( SELECT date, open, high, low, last, tvol from v_stock_intraday_chart WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec;
            sql += " UNION ALL SELECT date, open, high, low, last, tvol from v_stock_intraday_chart WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec +") x;";
        }
        else
        {            
            sql = "SELECT * from ( SELECT date, open, high, low, last, tvol from v_" +yr +viewName +"_chart_adjusted WHERE stock = '" +code +"' ";
            sql += " UNION ALL SELECT date, open, high, low, last, tvol from v_" +(yr-1) +viewName +"_chart_adjusted WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec +") x;";
        }
    }
    else
    {
        if (interval == 'd')
        {
            sql = "SELECT date_trunc('hour'::text, t.date) + (date_part('minute'::text, t.date)::integer / 5)::double precision * '00:05:00'::interval AS date, \
                    t.stock, (array_agg(t.open ORDER BY t.date))[1] AS open, max(t.high) AS high, min(t.low) AS low, \
                    (array_agg(t.last ORDER BY t.date DESC))[1] AS last, sum(t.tvol) AS tvol, sum(t.freq) AS freq \
                    FROM v_stock_intraday_chart t WHERE t.stock = '" +code +"' GROUP BY (date_trunc('hour'::text, t.date) + (date_part('minute'::text, t.date)::integer / 5)::double precision * '00:05:00'::interval), t.stock LIMIT " +maxRec;
        }
        else 
        {
            sql = "SELECT date, open, high, low, last, tvol from v_" +yr +viewName +"_chart_adjusted WHERE stock = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec;            
        }
    }
    
    //Date,Open,High,Low,Close,Volume
    return pgConnObj.client.query(sql).then(function (result)
    {
        if (result == undefined || result.rows == undefined || result.rows.length <= 0)                    
            return Promise.resolve([]);

        var jsonData = [], obj, momObj;
        for (var i = 0; i < result.rows.length; i++)
        {
            obj = {};
               
            momObj = moment(result.rows[i].date);
            if (momObj.isValid())
            {                
                obj.date = momObj.format("D-MMM-YY HH mm");
                obj.open = result.rows[i].open;
                obj.high = result.rows[i].high;
                obj.low = result.rows[i].low;
                obj.close = result.rows[i].last;
                obj.tvol = result.rows[i].tvol;
                jsonData.push(obj);
            }
        }        
       
        if (jsonData.length <= 0)
        {
            console.log("empty json data!");                        
        }
        else
        {            
            var tmp = jswindow.d3.select("body");
            tmp.select("svg").remove();            
            tmp.call(RSI(d3,techan, jsonData));
            return new Promise(function (resolve, reject) {
                svg2img(jswindow.d3.select("body").html(), function (error, buffer) {
                    if (error)
                        reject(error);

                    var dirName = slPrefix +"/";
                    var fileName = getSparklineFileName(code, interval, "");

                    return fsx.outputFile(dirName + fileName, buffer)
                        .then(() => { console.log(dirName + fileName); resolve();})
                        .catch( (err) => ( console.log(err) ));
                });
            });
        }        
    });    
}

//DRIVER Program
function generateAndCalcMACDCross(rsIParam)
{
    return client.delAsync("MACDCrossPos").then(function (macdCrossPosRes) {
        console.log("Done Cleaning up MACDCrossPos...");
        return client.delAsync("MACDCrossNeg").then(function (macdCrossPosNeg) {
            console.log("Done Cleaning up MACDCrossNeg...");
                return pgb.connect(conString)
                .then(function (connection) {
                    pgConnObj = connection;
                    return pgConnObj.client.query("select stock from stock_list order by stock");
                })
                .then(function (result) {
                    var code;
                    var stockList = [];        
                    
                    for (var i = 0; i < result.rows.length; i++)
                    {
                        code = result.rows[i].stock.trim();
                        if (code.indexOf("-W") < 0 && code.indexOf("-R") < 0)
                            stockList.push(code);
                    }
            
                    return stockList;
                })
                .then(function (stList) {
                    return fsx.remove(slPrefix +"/MACDCross").then(() => {
                        return stList.reduce(function (pm, key) {
                            return pm.then(function () {                
                                //We should maintain a priority list for priority stocks which included in lq45?
                                return loadAndFormatDataTALib(key, 200, 'MACDCross');                
                            })
                        }, Promise.resolve());
                    });
                })
                .then(function() {
                    console.log('Finished');        
                    pgConnObj.done();
            
                    //Delay to let the IO operation completes?
                    return Promise.resolve();
                })
                .catch(function (error)
                {
                    console.log(error);
                    return Promise.reject();
                });                
            });
        });    
}

function generateAndCalcRSI(rsIParam)
{
    return client.delAsync("RSIOB").then(function (obRes) {
        console.log("Done Cleaning up RSIOB...");
        return client.delAsync("RSIOS").then(function (osRes) {
            console.log("Done Cleaning up RSIOS...");
            return client.delAsync("RSINE").then(function (neRes) {
                console.log("Done Cleaning up RSINE...");
                return pgb.connect(conString)
                .then(function (connection) {
                    pgConnObj = connection;
                    return pgConnObj.client.query("select stock from stock_list order by stock");
                })
                .then(function (result) {
                    var code;
                    var stockList = [];
                    //var stockList = ["AALI"];
                            
                    for (var i = 0; i < result.rows.length; i++)
                    {
                        code = result.rows[i].stock.trim();
                        if (code.indexOf("-W") < 0 && code.indexOf("-R") < 0)
                            stockList.push(code);
                    }        
            
                    return stockList;
                })
                .then(function (stList) {
                    return fsx.remove(slPrefix +"/RSI/").then(() => {
                        return stList.reduce(function (pm, key) {
                            return pm.then(function () {
                                return loadAndFormatDataTALib(key, 100, 'RSI');
                            })
                        }, Promise.resolve());
                    });
                })
                .then(function() {
                    console.log('Finished');
                    pgConnObj.done();
            
                    //Delay to let the IO operation completes?
                    return Promise.resolve();
                })
                .catch(function (error)
                {
                    console.log(error);
                    return Promise.reject();
                });
            })
        })
    });   
}

//var function_desc = talib.explain("MACD");
//console.dir(function_desc);
/*
return generateAndCalcMACDCross().then(function () {
    sleep(7000).then(() => {
        console.log('Finished generating MACDCross');
        process.exit(0);  
    });
});
*/

function exitHandler(options, err) {
    if (options.cleanup) console.log('exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

if (process.argv[2] == 'schedule')
{
    console.log('Starting sl generator scheduler (2)...');
    
    var job1 = schedule.scheduleJob('50 6 * * 1-5', function() {
        generateAndCalcRSI().then(function () {
            console.log('Calculating RSI...');
            sleep(5000).then(() => {
                console.log('Finished generating RSI');                
            });
        });
    });

    var job2 = schedule.scheduleJob('55 6 * * 1-5', function() {
        generateAndCalcMACDCross().then(function () {
            sleep(5000).then( () => {
                console.log('Finished generating MACDCross');
                process.exit(0);
            });
        });
    });
}
else
{
    if (process.argv[2] == 'RSI')
    {
        console.log('Calculating RSI...');
        generateAndCalcRSI().then(function () {
            sleep(5000).then(() => {
                console.log('Finished generating RSI');
                process.exit(0);
            });            
        });
    }
    else
    if (process.argv[2] == 'MACDCross')
    {
        console.log('Calculating MACDCross...');  
        generateAndCalcMACDCross().then(function () {
            sleep(5000).then( () => {
                console.log('Finished generating MACDCross');
                process.exit(0);
            });
        });
    }
    else {
        console.log('Usage: node Generator.js RSI|MACDCross');
        process.exit(0);
    }
}

process.stdin.resume();//so the program will not close instantly

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
