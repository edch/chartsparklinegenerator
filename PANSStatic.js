
function PANSStatic(d3, techan, techanData) {
    var margin = {top: 20, right: 20, bottom: 30, left: 50},
        width = 600 - margin.left - margin.right,
        height = 350 - margin.top - margin.bottom;
  
    var parseDate = d3.timeParse("%d-%b-%y");
  
    var x = techan.scale.financetime()
      .range([0, width]);
  
    var y = d3.scaleLinear()
      .range([height, 0]);
  
    var candlestick = techan.plot.candlestick()
      .xScale(x)
      .yScale(y);

    var dataLen = techanData.length;  
    var data = techanData.map(function (d) {
        
      return {        
        date: parseDate(d.date),
        open: +d.open,
        high: +d.high,
        low: +d.low,
        close: +d.close,
        volume: +d.tvol
      };
    }).sort(function (a, b) {
      return d3.ascending(a.date, b.date);
    });

    //var firstDate = data[0].date, endDate = data[dataLen-1].date;
    var tickFormat = function(date) {
      if (date == null) return;
      if(date.getDay()&&date.getDate()!=1) return d3.timeFormat('%b')(date);
      if(date.getDate()!=1) return d3.timeFormat('%e %b')(date);
      return d3.timeFormat('%b %Y')(date);
    }

    var accessor = candlestick.accessor();
    var xAxis = d3.axisBottom(x).ticks(10).tickFormat(tickFormat);
    //var xAxis = d3.axisBottom(x);
    var yAxis = d3.axisLeft(y);
    
    return function(g) {
      var svg = g.append("svg")
          .attr("version", "1.1")
          .attr("xmlns", "http://www.w3.org/2000/svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
      x.domain(data.map(accessor.d));
      y.domain(techan.scale.plot.ohlc(data, accessor).domain());
  
      svg.append("g")
        .datum(data)
        .attr("class", "candlestick")
        .call(candlestick);
  
      svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);    
      
      svg.append("g")
          .attr("class", "y axis")
          .call(yAxis);
     
      svg.append("g")
        .append("text")
        .attr("x", 450)
        .attr("y", 20)
        .style("font-size", "32px")        
          .text("PANS");

        //CSS                
        svg.selectAll(".axis path, .axis line").style("fill", "none").style("stroke","#000").style("shape-rendering", "crispEdges");
        svg.selectAll("path.candle").style("stroke", "#000000");
        svg.selectAll("path.candle.body").style("stroke-width", "0");
        svg.selectAll("path.candle.up").style("stroke", "#00AA00").style("fill", "#004400");
        svg.selectAll("path.candle.down").style("stroke", "#FF0000").style("fill", "#AA0000");
    }
  }

  if(typeof module === 'object') {
    module.exports = PANSStatic;
  }