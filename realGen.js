var fsx = require('fs-extra');
var d3 = require('d3');
var schedule = require('node-schedule');
var Promise = require('bluebird');
var techan = require('techan');
var sparkline = require('./sparkline');
var jsdom = require('jsdom');
var svg2img = require('svg2img');
var moment = require('moment');
var d3File = fsx.readFileSync('d3.v4.min.js').toString();

//PG
var Pgb = require("pg-bluebird");
var pgb = new Pgb(); //test if this can be reused
var pgConnObj;
var conString = "postgres://datafeed:emptykosong@172.16.19.24/pgdb";

var jswindow;
jsdom.env({
    html: "<html><head><style></style></head><body></body></html>",
    src: [d3File],
    done:
        function (err, window) {
            if (err)
            {
                //reject(err);
                console.log("jsdom error: " +err);
                return;
            }
            
            //resolve(window);
            console.log("jsdom window created");
            jswindow = window;
        }
    });//end of env

var slPrefix = "./sparklines";

//Load data here
function loadAndFormatData(code, interval)
{
    if (!code || !interval)
        return Promise.resolve([]);   
        
    var maxRec, viewName;
    var dt = new Date();
    var nowMoment = moment();
    var yr = dt.getFullYear(), yr2;

    //Set the proper interval
    if (interval == 'd')
    {
        //Trading day per week is at most 5 days * 7 hours per day
        //Per jam selama 1 minggu?
        
        //maxRec = maxDays * 7;
        maxRec = 7; //approx 60 mins * 6 hrs, per 5 mins chart => 360 / 72

        //viewName = "_hourly";
        viewName = "_daily";        
    }
    else if (interval == 'm')
    {
        //Trading day sebulan
        maxRec = 22;
        //Daily selama sebulan terakhir //atau beginning from last month?
        viewName = "_daily";        
    }
    else if (interval == 'y')
    {
        //Approx trading day setahun
        maxRec = 200;
        //atau weekly?
        viewName = "_daily";       
    }
    else return Promise.resolve([]);
   
    sql = "SELECT date, open, high, low, last, vol from chart" +viewName +"_stock_adjusted WHERE stock_code = '" +code +"' ORDER BY DATE DESC LIMIT " +maxRec +";";   
    
    //Date,Open,High,Low,Close,Volume
    return pgConnObj.client.query(sql).then(function (result) 
    {
        if (result == undefined || result.rows == undefined || result.rows.length <= 0)
        {
            //console.log(sql);
            return Promise.resolve([]);
        }

        var jsonData = [], obj, momObj;
        for (var i = 0; i < result.rows.length; i++)
        {
            obj = {};
               
            momObj = moment(result.rows[i].date);
            if (momObj.isValid())
            {                
                obj.date = momObj.format("D-MMM-YY HH mm");
                obj.open = result.rows[i].open;
                obj.high = result.rows[i].high;
                obj.low = result.rows[i].low;
                obj.close = result.rows[i].last;
                obj.tvol = result.rows[i].tvol;
                jsonData.push(obj);
            }
        }
        
        /*
        return new Promise (function (resolve, reject) {
            
            jsdom.env({
            html: "<html><head><style></style></head><body></body></html>",
            src: [d3File],
            done:
                function (err, window) {
                    if (err)
                    {
                        reject(err);
                        return;
                    }
                    
                    resolve(window);
                }
            })//end of env          
        })
        */
        //.then(function (wnd) {
            if (jsonData.length <= 0)
            {
                console.log("empty json data!");
                //reject("empty json data");
            }
            else
            {
                var tmp = jswindow.d3.select("body");
                tmp.select("svg").remove();
                //jswindow.d3.select("body").call(sparkline(d3, techan, jsonData));                
                tmp.call(sparkline(d3,techan, jsonData));
                return new Promise(function (resolve, reject) {
                    svg2img(jswindow.d3.select("body").html(), function (error, buffer) {
                        if (error)
                            reject(error);

                        return fsx.outputFile(slPrefix +"/images/sl" +code +interval +'.png', buffer)
                            .then(() => { console.log(slPrefix +"/images/sl" +code +interval +'.png'); resolve();})
                            .catch( (err) => ( console.log(err) ));
                    });
                });
            }
        //})
        //.catch((err) => console.log("err4: " +err));
    });

    //return Promise.resolve([]);
}

//DRIVER Program
function generateSparklines(ivals)
{
    if (ivals.length < 1)
    {
        console.log("Invalid interval parameters!");
        return Promise.reject();
    }

    return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;
        return pgConnObj.client.query("select stock from stock_list order by stock");
    })
    .then(function (result) {
        var code;
        //var stockList = ['AALI'];
        
        var stockList = [];
        for (var i = 0; i < result.rows.length; i++)
        {
            code = result.rows[i].stock.trim();
            if (code.indexOf("-W") < 0 && code.indexOf("-R") < 0)
                stockList.push(code);
        }       

        return stockList;
    })
    .then(function (stList) {
        console.time("GenerateSL");
        return fsx.remove(slPrefix +"/images/").then(() => {
            console.log("Finished removing all png files!");
            return stList.reduce(function (pm, key) {
                return pm.then(function () {
                    return ivals.reduce(function(promise, iv) {
                        return promise.then(function () {
                            console.log(key +" " +iv);
                            return loadAndFormatData(key, iv);
                        })
                        .catch((err) => console.log("err3: " +err));
                    }, Promise.resolve()).then (function () {
                        console.log("Done processing " +key);
                    })
                    .catch((err) => console.log("err1: " +err))
                })
            }, Promise.resolve());
        })        
    })
    .then(function() {
        pgConnObj.done();
        console.timeEnd("GenerateSL");
        return Promise.resolve();
    })
    .catch(function (error)
    {
        console.log(error);
        return Promise.reject();
    });
}

function exitHandler(options, err) {    
    if (options.cleanup) console.log('exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

//loadAndFormatData('ASII', 'y');
//COMMAND LINE PARSERS
if (process.argv[2] == 'schedule')
{
    console.log('Starting sl generator scheduler (2)...');
    
    var jobsl = schedule.scheduleJob('0 7 * * 1-5', function() {
        console.log('Generating all sparklines...');
        generateSparklines(['d', 'm', 'y']).then(function () {        
            console.log('Finished generating sparklines');
        });
    });    
}
else
{
	if (process.argv[2] == 'GenerateAll')
	{
        console.log('Generating all sparklines...');  
        generateSparklines(['d', 'm', 'y']).then(function () {
        //generateSparklines(['y']).then(function () {
            console.log('Finished generating sparklines');
            process.exit(0);
        });
    }
    else if (process.argv[2] == 'GenerateIntradayOnly')
    {
        console.log('Generating all intraday sparkline...');
        return generateSparklines(['d']).then(function () {
            console.log('Finished generating intraday sparklines');
            process.exit(0);
        });
    }    
    else 
    {
        console.log('Usage: node realGen.js ( GenerateAll|GenerateIntradayOnly )');
        console.log("Invalid argument! Program will now exit...");
        process.exit(0); 
    }
}

process.stdin.resume();//so the program will not close instantly

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
