var Pgb = require("pg-bluebird");
var schedule = require('node-schedule');
var Promise = require('bluebird')

//TECHNICAL ANALYSIS LIBRARY
var talib = require('./node_modules/talib/build/Release/talib');
//var talibProm = require('talib-promise');
console.log("TALib Version: " + talib.version);

/*PG CONF*/
var pgb = new Pgb(); //test if this can be reused
var conString = "postgres://datafeed:emptykosong@172.16.19.24/pgdb";
var pgConnObj;

/*REDIS CONF*/
var redis = require('redis');
Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
client = redis.createClient(); //Select default database (0)

client.on("error", function (err) {
    console.log("Redis Error: " + err);
});

var winston = require('winston');
var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
            'timestamp':function () {
                return formatDate(new Date());
            }, 
            'colorize':true
        })
      //,new (winston.transports.File)({ filename: 'somefile.log' }) 
    ]
});

var oneSec = 1000;
var oneMin = 60 * oneSec;
var fiveMin = 5 * oneMin;
var fifteenMin = 15 * oneMin;
var halfHour = 30 * oneMin;
var oneHour = 60 * oneMin;
var oneDay = 1440 * oneMin;
var sessTimeout = 36000; //10 hour-expiry period

//STD functions, logging and date formatting for logging
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
}

function formatDateForPG(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();

  hours = hours < 10 ? '0' +hours : hours;
  minutes = minutes < 10 ? '0' +minutes : minutes;
  seconds = seconds < 10 ? '0' +seconds : seconds;

  var strTime = hours + ':' + minutes +':' + seconds;
  var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate() + " " + strTime;
}

function GetDateOnly(date) {
	var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate();
}

function Calc_StocksUpDown(RGOnly)
{
    var arr = [];

    logger.info("Calc_StocksUp ... ");

    //connect
    return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;

        return pgConnObj.client.query("select stock from stock_list order by stock");
    })
    .then(function (result) {
            var code;
            for (var i = 0; i < result.rows.length; i++)
            {
                if (RGOnly)
                {
                    code = result.rows[i].stock.trim();
                    if (code.indexOf("-W") < 0 && code.indexOf("-R") < 0)
                        arr.push(code);
                }
            }

            return arr;
        })
    .then(function (arr) {
        var ival = [2,3,5,8], arrKey = [];
    
        for (var m = 0; m < ival.length; m++)
        {                    
            arrKey.push("ConsUp:" +ival[m]);
            arrKey.push("ConsDown:" +ival[m]);
        }

        console.time("StocksUpDown");        
        
        var x = arrKey.map(function(keyName) {
            //delete existing redis keys first
            return client.delAsync(keyName);
        });
        
        //console.log(x);
        var y = Promise.all(x);
        
        return y.then(function(data) {
            //console.log(data);
            console.log("Finished deleting. Now processing.");
            
            //arr.reduce(function (promise, code) {
            return Promise.each(arr, function(code, index, length) {
                console.log(code);
                var dt = new Date(), maxDays = 8;
                var yr = dt.getFullYear();
                var sql = '';
                dt.setDate( dt.getDate() - maxDays);                
                
                sql += "SELECT stock_code, open, last from chart_daily_stock_adjusted WHERE stock_code = '" +code +"' ORDER BY DATE DESC LIMIT " +maxDays;
                
                return pgConnObj.client.query(sql).then(function (result) {                       
                        
                        if (result.rows.length > 0) 
                        {                    
                            console.log("Calculating " +result.rows[0].stock_code.trim());
                            //Calc for 2,3,5,8 days
                            var consUp = true, consDown = true, k, key;
                            
                            //Interval Loop
                            for (var j = 0; j < ival.length; j++ )
                            {
                                if (j == 0)
                                    k = 0;
                                else 
                                    k = ival[j-1];
        
                                if (result.rows.length < ival[j])
                                    break;
        
                                //Checker loop
                                for (; k < ival[j]; k++)
                                {
                                    if ((result.rows[k].open < result.rows[k].last) && consUp) //up                                                         
                                        consDown = false;
                                    else if ((result.rows[k].open > result.rows[k].last) && consDown) //down
                                        consUp = false;
                                    else 
                                    {
                                        consUp = false;
                                        consDown = false;
                                    }
                                }
        
                                if (consUp == false && consDown == false)
                                {
                                    console.log("breaking at " +ival[j]);
                                    break;
                                }
        
                                if (consUp == true)
                                    key = "ConsUp:";
                                else key = "ConsDown:";
                               
                                client.lpushAsync(key +ival[j], result.rows[0].stock_code.trim()).then(function (redPushRes) {
                                    console.log("Finished inserting " +result.rows[0].stock_code.trim() +" " +ival[j]);
                                });
                            }   
                        }             
                    });//end of pgconnobj.client.query
                });//end of promise.each
            }); //end of y.then
        })
    .then(() => {
        console.timeEnd("StocksUpDown");
    });//end of function(arr)
} //end of function

//5, 20, 50, 100, 200
function Calc_NDayWinLose()
{    
    var arr = [];
    var intervals = [5, 20, 50, 100, 200];

    return pgb.connect(conString)
    .then(function (connection) {
        pgConnObj = connection;
        
        return pgConnObj.client.query("select stock from stock_list order by stock");
    })
    .then(function (result) {
        var code;
        for (var i = 0; i < result.rows.length; i++)
        {
            code = result.rows[i].stock.trim();
            if (code.indexOf("-W") < 0 && code.indexOf("-R") < 0)
                arr.push(code);
        }

        return Promise.resolve();
    })
    .then(function ()
    {
        var arrKey = [];

        for (var m = 0; m < intervals.length; m++)
        {                    
            arrKey.push("NDayWin:" +intervals[m]);
            arrKey.push("NDayLose:" +intervals[m]);
        }

        console.time("NDayWinLose");        
        
        var x = arrKey.map(function(keyName) {            
            return client.delAsync(keyName);
        });
        
        var y = Promise.all(x);
        //return Promise.each(arr, function(code, index, length) 
        return y.then(function(data) {
            var dt = new Date(), maxDays = intervals[intervals.length-1] + 1;
            var yr = dt.getFullYear();
            dt.setDate( dt.getDate() - maxDays);
            var yr2 =  dt.getFullYear();
            
            console.time("Calc_NDayWinLose");

            //LAYER 1            
            return Promise.all(Promise.map(arr, function (x) {
                //Untuk semua kode saham, kita ambil record sebanyak maxInterval                
                var code = x, sql = '';
                
                sql += "SELECT last as LAST from chart_daily_stock_adjusted WHERE stock_code = '" +code +"' ORDER BY DATE DESC LIMIT " +maxDays;
                
                //LAYER 2
                return pgConnObj.client.query(sql).then(function (result) {
    
                    //We have result set and code
                    var arrData = [];
            
                    for (var j = result.rows.length-1; j >= 0; j--)
                        arrData.push(result.rows[j].last);
                    
                    if (result.rows.length <= 0)
                    {
                        return Promise.resolve(-1); //reject?
                    }
    
                    //LAYER 3
                    return Promise.all(Promise.map(intervals, function (inter) {
                        var timeFrame;
    
                        if (arrData.length > inter)
                            timeFrame = inter;
                        else
                        {
                            //timeFrame = arrData.length;
                            console.log(code +" " +arrData.length +" vs " +inter);
                            return Promise.resolve(-1); //reject?
                        }
                        
                        //Now we also have timeFrame/interval
                        return new Promise(function (resolve, reject) {
                            //console.log(code +" " +timeFrame);
                            talib.execute({
                                name: "ROCP",
                                inReal: arrData,
                                startIdx: arrData.length-timeFrame,
                                endIdx: arrData.length-1,
                                optInTimePeriod: timeFrame
                            },
                            function (err, result) {
                                if (err)
                                {
                                    console.log(err);
                                    return reject(err);
                                }
                                resolve(result);
                            });
                        }).then( function (data) {
                            if (data.result.outReal.length > 0)
                            {                            
                                //console.log(code +" " +timeFrame +" " +data.result.outReal[data.result.outReal.length-1]);
                                return client.zaddAsync("NWinLose:" +timeFrame, +data.result.outReal[data.result.outReal.length-1], code);
                            }
                            else return Promise.reject("Failed inserting to redis for " +code +"-" +timeFrame);
                        });
                    })); //end of return
                })
            }))
            .then(function () {           
                console.log("All Finished!");
                console.timeEnd("Calc_NDayWinLose");
            });    
        });
    });   
}

//Clean up and exit handler
process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
    
    if (options.cleanup) logger.log('info', 'exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

if (process.argv[2] == 'schedule')
{
    logger.log('info', 'Starting scheduler (2)...');
    
    //NOTE: semua sparkline akan digenerate jam 07 tepat!
    var jobsl = schedule.scheduleJob('55 6 * * 1-5', function() {
        console.log('Calculating stocks up down...');  
        return Calc_StocksUpDown(true).then(function () {
            console.log('Finished Calculating stocks up down');        
        });
    });

    var jobs2 = schedule.scheduleJob('56 6 * * 1-5', function() {
        console.log('Calculating n day win lose...');  
        Calc_NDayWinLose().then(function () {
            console.log('Finished Calculating n day win lose');
        });
    });
}
else
{
	if (process.argv[2] == 'StocksUpDown')
	{        
        logger.log('info', 'Performing direct update...');
        return Calc_StocksUpDown(true).then(function () {
            console.log('Finished Calculating StocksUpDown');
            process.exit(0);
        });    	
    }
    else if (process.argv[2] == 'NDayWinLose')
    {
        logger.log('info', 'Performing direct update...');
        return Calc_NDayWinLose().then(function () {
            console.log('Finished Calculating n day win lose');
            process.exit(0);
        });        
    }
    else 
    {
        logger.log('Usage: node manager.js ( StocksUpDown|NDayWinLose )');
        logger.log("Invalid argument! Program will now exit...");
        process.exit(0); 
    }    

    //process.exit(0); 
}

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
