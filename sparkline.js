function sparkline(d3, techan, jsonData, usrHeight, usrWidth, strokeColor) {

  var defW = 55, defH = 25, defStrokeColor = "#4582E7";

  if (usrHeight != undefined)
    defH = usrHeight;

  if (usrWidth != undefined)
    defW = usrWidth;

  if (strokeColor != undefined)
    defStrokeColor = strokeColor;

  var margin = {top: 0, right: 0, bottom: 0, left: 0},
      width = defW - margin.left - margin.right,
      height = defH - margin.top - margin.bottom;  
    
  var parseDate = d3.timeParse("%d-%b-%Y %H %M");
  //var parseDate = d3.timeParse("%Y-%m-%dT%H:%M:%S.%LZ");

  var dataLen = jsonData.length;

  var x = techan.scale.financetime()
    .range([0, width])
    //.range([0, (width/72) * dataLen])
    .outerPadding(0);

  var y = d3.scaleLinear()
    .range([height, 0]);

  var close = techan.plot.close()
    .xScale(x)
    .yScale(y)    

  var accessor = close.accessor();
  var xAxis = d3.axisBottom(x);
  var yAxis = d3.axisLeft(y);
  var data = jsonData.map(function (d) {
    return {        
      date: parseDate(d.date),
      open: +d.open,
      high: +d.high,
      low: +d.low,
      close: +d.close,
      volume: +d.tvol
    };
  }).sort(function (a, b) {
    return d3.ascending(a.date, b.date);
  });

  return function(g) {
    var svg = g.append("svg")
        .attr("version", "1.1")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g").attr("class", "close");

    svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")");

    svg.append("g").attr("class", "y axis")
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Price ($)");

    x.domain(data.map(close.accessor().d));
    y.domain(techan.scale.plot.ohlc(data, close.accessor()).domain());

    svg.selectAll("g.close").datum(data).call(close);
    svg.selectAll("g.x.axis").call(xAxis);
    svg.selectAll("g.y.axis").call(yAxis);

    svg.selectAll("path.line").style("fill", "none").style("stroke",defStrokeColor).style("stroke-width", "1");
    //svg.select("path.zero").style("stroke", "#555555").style("stroke-dasharray", "0.7");
    svg.selectAll("g.y.axis").style("display", "none");
    svg.selectAll("g.x.axis").style("display", "none");
  }
}

// If we're in node
if(typeof module === 'object') {
  module.exports = sparkline;
}